//
//  ViewController.m
//  technicaltest
//
//  Created by Prince Prabhakar on 08/10/15.
//  Copyright (c) 2015 Prince Prabhakar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    
    IBOutlet UIImageView *dp;
    IBOutlet UILabel *personalInfo;
    IBOutlet UITextField *firstName;
    IBOutlet UITextField *lastName;
    IBOutlet UITextField *eMail;
    IBOutlet UITextField *contactNumber;
    IBOutlet UIButton *done;
    IBOutlet UIButton *change;
 
    
}
@end

@implementation ViewController



- (void)viewDidLoad {
  
[super viewDidLoad];
    
    dp.clipsToBounds = YES;
    dp.layer.cornerRadius = dp.frame.size.width/2;
    
    [self.view addSubview:dp];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

        -(BOOL)CheckCharacter:(NSString *)text{
            
            NSCharacterSet *ch=[[NSCharacterSet characterSetWithCharactersInString:@"qwertyuiopasdfghjklzxcvbnm"]invertedSet];
            
            if([text rangeOfCharacterFromSet:ch].location==NSNotFound)
                
            {
                
                NSLog(@"No Special character");}
            
            else
                
            {
                
                NSLog(@"Special character found");
                
                return YES;
                
            }
            
            return NO;
            
        }
        
        -(BOOL)CheckNumber:(NSString *)text{
            
            NSCharacterSet *num=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]invertedSet];
            
            if([text rangeOfCharacterFromSet:num].location==NSNotFound)
                
            {
                
                NSLog(@"No character");}
            
            else
                
            {
                
                NSLog(@"character found");
                
                return YES;
                
            }
            
            return NO;
            
        }
        
        
        
        
        
        
        
- (IBAction)doneButton:(id)sender {
    if ([self CheckCharacter:firstName.text]==YES)
        
    {
        
        [self alert1];
        
    }
    
    
    
    else if ([self CheckCharacter:lastName.text]==YES){
        
        [self alert2];
        
    }
    
    else if ([self CheckNumber:contactNumber.text]==YES)
        
    {
        
        [self alertnum];
        
    }
    
    else if ([firstName.text length]==0 || [lastName.text length]==0 || [eMail.text length]==0 || [contactNumber.text length ]==0)
        
    {
        
        [self alertCheckFields];
        
        
        
    }
    
    
    else
        
    {
        
        [self alertSubmit];
        
    }
}















        -(void)alert1{
            
            
            
            UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"first name" message:@"only characters are allowed" delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:Nil, nil];
            
            [alert1 show];
            
            
            
        }
        
        
        
        
        
        
        
        -(void)alert2{
            
            
            
            UIAlertView *alert2=[[UIAlertView alloc]initWithTitle:@"last name" message:@"only characters are allowed" delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:Nil, nil];
            
            [alert2 show];
            
            
            
        }
        
        -(void)alertnum{
            
            UIAlertView *alertnum=[[UIAlertView alloc]initWithTitle:@"phone number" message:@"characters found" delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:Nil, nil];
            
            [alertnum show];
            
            
            
            
            
            
            
        }
        
        -(void)alertSubmit
        
        {
            
            UIAlertView *alertSubmit=[[UIAlertView alloc]initWithTitle:@"Congratulations!!" message:@"user successfully registered" delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:Nil, nil];
            
            [alertSubmit show];
            
        }
        
        
        
        -(void)alertCheckFields{
            UIAlertView *alertCheckFields=[[UIAlertView alloc]initWithTitle:@"oops!!" message:@"complete your information " delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:Nil, nil];
            
            [alertCheckFields show];
            
        }
        
        -(void)alertCheckPassword{
            
            UIAlertView *alertCheckPassword=[[UIAlertView alloc]initWithTitle:@"oops!" message:@"not matching " delegate:Nil cancelButtonTitle:@"cancel" otherButtonTitles:Nil, nil];
            
            [alertCheckPassword show];
            
        }


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
